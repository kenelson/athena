
/*
   Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/
#include "../PRDxAODConvertorAlg.h"
#include "../xRpcMeasToTrkPrdCnvAlg.h"
DECLARE_COMPONENT( Muon::PRDxAODConvertorAlg )
DECLARE_COMPONENT( MuonR4::xRpcMeasToRpcTrkPrdCnvAlg)